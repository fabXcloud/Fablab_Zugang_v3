EESchema Schematic File Version 4
LIBS:m5stackoutput_v2-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L m5stackoutput_v2-rescue:Conn_02x15_Odd_Even-Connector_Generic J1
U 1 1 5CA1159B
P 1650 1650
F 0 "J1" H 1700 2567 50  0000 C CNN
F 1 "Conn_02x15_Odd_Even" H 1700 2476 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x15_P2.54mm_Vertical" H 1650 1650 50  0001 C CNN
F 3 "~" H 1650 1650 50  0001 C CNN
	1    1650 1650
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR01
U 1 1 5CA1165B
P 1000 1000
F 0 "#PWR01" H 1000 750 50  0001 C CNN
F 1 "GND" H 1005 827 50  0000 C CNN
F 2 "" H 1000 1000 50  0001 C CNN
F 3 "" H 1000 1000 50  0001 C CNN
	1    1000 1000
	1    0    0    -1  
$EndComp
Text GLabel 2450 2350 2    50   Input ~ 0
Bat
Wire Wire Line
	1950 2250 2450 2250
Wire Wire Line
	2450 2350 1950 2350
Wire Notes Line
	3350 600  3350 2600
Wire Notes Line
	650  600  650  2600
Text Notes 2250 850  0    50   ~ 0
J1 Connector\nHeader Connector
$Comp
L m5stackoutput_v2-rescue:Conn_01x10-Connector_Generic J3
U 1 1 5CA12190
P 1700 3450
F 0 "J3" H 1780 3442 50  0000 L CNN
F 1 "Conn_01x08" H 1780 3351 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x05_P2.54mm_Vertical" H 1700 3450 50  0001 C CNN
F 3 "~" H 1700 3450 50  0001 C CNN
	1    1700 3450
	1    0    0    -1  
$EndComp
Text Notes 1000 5800 0    50   ~ 0
5V output\nFed from 5V Reg 2A max with ESP
Wire Notes Line
	650  2750 2400 2750
Wire Notes Line
	2400 2750 2400 4250
Wire Notes Line
	2400 4250 650  4250
Wire Notes Line
	650  4250 650  2750
$Comp
L m5stackoutput_v2-rescue:Conn_01x10-Connector_Generic J2
U 1 1 5CA119D7
P 1700 5100
F 0 "J2" H 1780 5092 50  0000 L CNN
F 1 "Conn_01x08" H 1780 5001 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x05_P2.54mm_Vertical" H 1700 5100 50  0001 C CNN
F 3 "~" H 1700 5100 50  0001 C CNN
	1    1700 5100
	1    0    0    -1  
$EndComp
Text GLabel 1050 4800 0    50   Input ~ 0
GND
Wire Wire Line
	1050 4800 1500 4800
Text GLabel 1050 5300 0    50   Input ~ 0
Q5-D
Text GLabel 10000 3200 3    50   Input ~ 0
Q6-D
Wire Wire Line
	10000 3050 10000 3100
Wire Wire Line
	1050 4700 1500 4700
Text GLabel 1050 5100 0    50   Input ~ 0
Q6-D
Text GLabel 1050 5000 0    50   Input ~ 0
GND
Wire Wire Line
	10000 2400 10000 2650
Wire Wire Line
	1050 5000 1500 5000
Text GLabel 1050 5200 0    50   Input ~ 0
GND
Wire Wire Line
	1050 5200 1500 5200
Text GLabel 1050 5400 0    50   Input ~ 0
GND
Wire Wire Line
	1050 4900 1500 4900
Text GLabel 1050 4900 0    50   Input ~ 0
Q7-D
Wire Wire Line
	1050 5100 1500 5100
Text GLabel 1050 4700 0    50   Input ~ 0
Q8-D
Wire Wire Line
	1050 5300 1500 5300
Text GLabel 9550 2850 0    50   Input ~ 0
Q6-G
Wire Wire Line
	9550 2850 9700 2850
Text GLabel 10000 1900 3    50   Input ~ 0
Q5-D
Wire Wire Line
	10000 1750 10000 1800
Wire Wire Line
	10000 1100 10000 1350
Text GLabel 9550 1550 0    50   Input ~ 0
Q5-G
Wire Wire Line
	9550 1550 9700 1550
Text GLabel 10000 4400 3    50   Input ~ 0
Q7-D
Wire Wire Line
	10000 4250 10000 4300
Wire Wire Line
	10000 3600 10000 3850
Text GLabel 9550 4050 0    50   Input ~ 0
Q7-G
Wire Wire Line
	9550 4050 9700 4050
Text GLabel 10000 5550 3    50   Input ~ 0
Q8-D
Wire Wire Line
	10000 5400 10000 5450
Wire Wire Line
	10000 4750 10000 5000
Text GLabel 9550 5200 0    50   Input ~ 0
Q8-G
Wire Wire Line
	9550 5200 9700 5200
Wire Notes Line
	650  4400 2400 4400
Wire Notes Line
	2400 4400 2400 5900
Wire Notes Line
	2400 5900 650  5900
Wire Notes Line
	650  5900 650  4400
$Comp
L m5stackoutput_v2-rescue:Conn_01x10-Connector_Generic J5
U 1 1 5CA72502
P 1700 6800
F 0 "J5" H 1780 6792 50  0000 L CNN
F 1 "Conn_01x08" H 1780 6701 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x05_P2.54mm_Vertical" H 1700 6800 50  0001 C CNN
F 3 "~" H 1700 6800 50  0001 C CNN
	1    1700 6800
	1    0    0    -1  
$EndComp
Wire Notes Line
	650  6100 2400 6100
Wire Notes Line
	2400 6100 2400 7600
Wire Notes Line
	2400 7600 650  7600
Wire Notes Line
	650  7600 650  6100
Wire Notes Line
	3250 2800 6550 2800
Text Notes 4000 3000 0    50   ~ 0
I2C Port Expander 8 Bit each
$Comp
L m5stackoutput_v2-rescue:MCP23008-m5stack-m5stackoutput-rescue U1
U 1 1 5CA79F23
P 5200 3850
F 0 "U1" H 5300 4500 50  0000 C CNN
F 1 "MCP23008" H 5450 4400 50  0000 C CNN
F 2 "Package_SO:SSOP-20_5.3x7.2mm_P0.65mm" H 5200 3850 50  0001 C CNN
F 3 "" H 5200 3850 50  0001 C CNN
	1    5200 3850
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:MCP23008-m5stack-m5stackoutput-rescue U2
U 1 1 5CA7B9BB
P 5200 5500
F 0 "U2" H 5300 6150 50  0000 C CNN
F 1 "MCP23008" H 5450 6050 50  0000 C CNN
F 2 "Package_SO:SSOP-20_5.3x7.2mm_P0.65mm" H 5200 5500 50  0001 C CNN
F 3 "" H 5200 5500 50  0001 C CNN
	1    5200 5500
	1    0    0    -1  
$EndComp
Text GLabel 4750 3100 0    50   Input ~ 0
5V
Wire Wire Line
	5200 3100 5200 3250
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR05
U 1 1 5CA8538A
P 5200 6100
F 0 "#PWR05" H 5200 5850 50  0001 C CNN
F 1 "GND" H 5205 5927 50  0000 C CNN
F 2 "" H 5200 6100 50  0001 C CNN
F 3 "" H 5200 6100 50  0001 C CNN
	1    5200 6100
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR04
U 1 1 5CA853C2
P 5200 4450
F 0 "#PWR04" H 5200 4200 50  0001 C CNN
F 1 "GND" H 5205 4277 50  0000 C CNN
F 2 "" H 5200 4450 50  0001 C CNN
F 3 "" H 5200 4450 50  0001 C CNN
	1    5200 4450
	1    0    0    -1  
$EndComp
Text GLabel 4600 3450 0    50   Input ~ 0
SCL
Text GLabel 4600 5100 0    50   Input ~ 0
SCL
Text GLabel 4600 5200 0    50   Input ~ 0
SDA
Text GLabel 4600 3550 0    50   Input ~ 0
SDA
Wire Wire Line
	4600 3550 4800 3550
Wire Wire Line
	4600 3450 4800 3450
Wire Wire Line
	4600 5100 4800 5100
Wire Wire Line
	4600 5200 4800 5200
Text GLabel 4600 3950 0    50   Input ~ 0
5V
Text GLabel 4600 5600 0    50   Input ~ 0
5V
Wire Wire Line
	4600 3950 4800 3950
Text GLabel 5850 4150 2    50   Input ~ 0
Q1-G
Text GLabel 5850 4050 2    50   Input ~ 0
Q2-G
Text GLabel 5850 3850 2    50   Input ~ 0
Q4-G
Text GLabel 5850 3950 2    50   Input ~ 0
Q3-G
Text GLabel 5850 3750 2    50   Input ~ 0
Q5-G
Text GLabel 5850 3650 2    50   Input ~ 0
Q6-G
Text GLabel 5850 3550 2    50   Input ~ 0
Q7-G
Text GLabel 5850 3450 2    50   Input ~ 0
Q8-G
Wire Wire Line
	5600 3450 5850 3450
Wire Wire Line
	5600 3550 5850 3550
Wire Wire Line
	5600 3650 5850 3650
Wire Wire Line
	5850 3750 5600 3750
Wire Wire Line
	5600 3850 5850 3850
Wire Wire Line
	5600 3950 5850 3950
Wire Wire Line
	5600 4050 5850 4050
Wire Wire Line
	5600 4150 5850 4150
Text GLabel 1050 6400 0    50   Input ~ 0
In1
Text GLabel 1050 6500 0    50   Input ~ 0
In2
Text GLabel 1050 6600 0    50   Input ~ 0
In3
Text GLabel 1050 6700 0    50   Input ~ 0
In4
Text GLabel 1050 6800 0    50   Input ~ 0
In5
Text GLabel 1050 6900 0    50   Input ~ 0
In6
Text GLabel 1050 7000 0    50   Input ~ 0
In7
Wire Wire Line
	1050 6400 1500 6400
Wire Wire Line
	1050 6500 1500 6500
Wire Wire Line
	1050 6600 1500 6600
Wire Wire Line
	1050 6700 1500 6700
Wire Wire Line
	1050 6800 1500 6800
Wire Wire Line
	1050 6900 1500 6900
Wire Wire Line
	1050 7000 1500 7000
Text GLabel 5850 5800 2    50   Input ~ 0
In1
Text GLabel 5850 5700 2    50   Input ~ 0
In2
Text GLabel 5850 5600 2    50   Input ~ 0
In3
Text GLabel 5850 5500 2    50   Input ~ 0
In4
Text GLabel 5850 5400 2    50   Input ~ 0
In5
Text GLabel 5850 5300 2    50   Input ~ 0
In6
Text GLabel 5850 5200 2    50   Input ~ 0
In7
Wire Wire Line
	5600 5200 5850 5200
Wire Wire Line
	5600 5300 5850 5300
Wire Wire Line
	5600 5400 5850 5400
Wire Wire Line
	5600 5500 5850 5500
Wire Wire Line
	5600 5600 5850 5600
Wire Wire Line
	5600 5700 5850 5700
Wire Wire Line
	5600 5800 5850 5800
Text GLabel 1050 7100 0    50   Input ~ 0
GND
Wire Wire Line
	1050 7100 1400 7100
Text GLabel 1100 1750 0    50   Input ~ 0
SDA
Text GLabel 2250 1750 2    50   Input ~ 0
SCL
Wire Wire Line
	1100 1750 1450 1750
Wire Wire Line
	2250 1750 1950 1750
$Comp
L m5stackoutput_v2-rescue:R-Device R5
U 1 1 5CA2BC46
P 9700 1300
F 0 "R5" H 9770 1346 50  0000 L CNN
F 1 "10k" H 9770 1255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9630 1300 50  0001 C CNN
F 3 "~" H 9700 1300 50  0001 C CNN
	1    9700 1300
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:R-Device R6
U 1 1 5CA36992
P 9700 2600
F 0 "R6" H 9770 2646 50  0000 L CNN
F 1 "10k" H 9770 2555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9630 2600 50  0001 C CNN
F 3 "~" H 9700 2600 50  0001 C CNN
	1    9700 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 2400 9700 2400
Wire Wire Line
	9700 2450 9700 2400
Connection ~ 9700 2400
Wire Wire Line
	9700 2400 10000 2400
Wire Wire Line
	9700 2750 9700 2850
Wire Wire Line
	9500 1100 9700 1100
Wire Wire Line
	9700 1150 9700 1100
Connection ~ 9700 1100
Wire Wire Line
	9700 1100 10000 1100
Wire Wire Line
	9700 1450 9700 1550
$Comp
L m5stackoutput_v2-rescue:R-Device R7
U 1 1 5CA578E0
P 9700 3800
F 0 "R7" H 9770 3846 50  0000 L CNN
F 1 "10k" H 9770 3755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9630 3800 50  0001 C CNN
F 3 "~" H 9700 3800 50  0001 C CNN
	1    9700 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 3600 9700 3600
Wire Wire Line
	9700 3600 9700 3650
Connection ~ 9700 3600
Wire Wire Line
	9700 3600 10000 3600
Wire Wire Line
	9700 3950 9700 4050
Wire Wire Line
	9550 4750 9700 4750
$Comp
L m5stackoutput_v2-rescue:R-Device R8
U 1 1 5CA73DC3
P 9700 4950
F 0 "R8" H 9770 4996 50  0000 L CNN
F 1 "10k" H 9770 4905 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 9630 4950 50  0001 C CNN
F 3 "~" H 9700 4950 50  0001 C CNN
	1    9700 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 4800 9700 4750
Connection ~ 9700 4750
Wire Wire Line
	9700 4750 10000 4750
Wire Wire Line
	9700 5100 9700 5200
Text GLabel 8100 3200 3    50   Input ~ 0
Q2-D
Wire Wire Line
	8100 3050 8100 3100
Wire Wire Line
	8100 2400 8100 2650
Text GLabel 7650 2850 0    50   Input ~ 0
Q2-G
Wire Wire Line
	7650 2850 7800 2850
Text GLabel 8100 1900 3    50   Input ~ 0
Q1-D
Wire Wire Line
	8100 1750 8100 1800
Wire Wire Line
	8100 1100 8100 1350
Text GLabel 7650 1550 0    50   Input ~ 0
Q1-G
Wire Wire Line
	7650 1550 7800 1550
Text GLabel 8100 4400 3    50   Input ~ 0
Q3-D
Wire Wire Line
	8100 4250 8100 4300
Wire Wire Line
	8100 3600 8100 3850
Text GLabel 7650 4050 0    50   Input ~ 0
Q3-G
Wire Wire Line
	7650 4050 7800 4050
Text GLabel 8100 5550 3    50   Input ~ 0
Q4-D
Wire Wire Line
	8100 5400 8100 5450
Wire Wire Line
	8100 4750 8100 5000
Text GLabel 7650 5200 0    50   Input ~ 0
Q4-G
Wire Wire Line
	7650 5200 7800 5200
$Comp
L m5stackoutput_v2-rescue:R-Device R1
U 1 1 5CA317E0
P 7800 1300
F 0 "R1" H 7870 1346 50  0000 L CNN
F 1 "10k" H 7870 1255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7730 1300 50  0001 C CNN
F 3 "~" H 7800 1300 50  0001 C CNN
	1    7800 1300
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:R-Device R2
U 1 1 5CA317E7
P 7800 2600
F 0 "R2" H 7870 2646 50  0000 L CNN
F 1 "10k" H 7870 2555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7730 2600 50  0001 C CNN
F 3 "~" H 7800 2600 50  0001 C CNN
	1    7800 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 2400 7800 2400
Wire Wire Line
	7800 2450 7800 2400
Connection ~ 7800 2400
Wire Wire Line
	7800 2400 8100 2400
Wire Wire Line
	7800 2750 7800 2850
Wire Wire Line
	7600 1100 7800 1100
Wire Wire Line
	7800 1150 7800 1100
Connection ~ 7800 1100
Wire Wire Line
	7800 1100 8100 1100
Wire Wire Line
	7800 1450 7800 1550
$Comp
L m5stackoutput_v2-rescue:R-Device R3
U 1 1 5CA317FA
P 7800 3800
F 0 "R3" H 7870 3846 50  0000 L CNN
F 1 "10k" H 7870 3755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7730 3800 50  0001 C CNN
F 3 "~" H 7800 3800 50  0001 C CNN
	1    7800 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 3600 7800 3600
Wire Wire Line
	7800 3600 7800 3650
Connection ~ 7800 3600
Wire Wire Line
	7800 3600 8100 3600
Wire Wire Line
	7800 3950 7800 4050
Wire Wire Line
	7650 4750 7800 4750
$Comp
L m5stackoutput_v2-rescue:R-Device R4
U 1 1 5CA31808
P 7800 4950
F 0 "R4" H 7870 4996 50  0000 L CNN
F 1 "10k" H 7870 4905 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 7730 4950 50  0001 C CNN
F 3 "~" H 7800 4950 50  0001 C CNN
	1    7800 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 4800 7800 4750
Connection ~ 7800 4750
Wire Wire Line
	7800 4750 8100 4750
Wire Wire Line
	7800 5100 7800 5200
Text GLabel 1050 3150 0    50   Input ~ 0
GND
Wire Wire Line
	1050 3150 1500 3150
Text GLabel 1050 3050 0    50   Input ~ 0
Q1-D
Wire Wire Line
	1050 3050 1500 3050
Text GLabel 1050 3250 0    50   Input ~ 0
Q2-D
Text GLabel 1050 3350 0    50   Input ~ 0
GND
Wire Wire Line
	1050 3350 1500 3350
Text GLabel 1050 3550 0    50   Input ~ 0
GND
Wire Wire Line
	1050 3550 1500 3550
Text GLabel 1050 3750 0    50   Input ~ 0
GND
Wire Wire Line
	1050 3250 1500 3250
Text GLabel 1050 3450 0    50   Input ~ 0
Q3-D
Wire Wire Line
	1050 3450 1500 3450
Text GLabel 1050 3650 0    50   Input ~ 0
Q4-D
Wire Wire Line
	1050 3650 1500 3650
Text Notes 1050 4200 0    50   ~ 0
5V output\nFed from 5V Reg 2A max with ESP
Wire Notes Line
	650  600  3350 600 
Wire Notes Line
	650  2600 3350 2600
$Comp
L m5stackoutput_v2-rescue:R-Device R12
U 1 1 5CA7B2E0
P 8400 5600
F 0 "R12" H 8470 5646 50  0000 L CNN
F 1 "100k" H 8470 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8330 5600 50  0001 C CNN
F 3 "~" H 8400 5600 50  0001 C CNN
	1    8400 5600
	1    0    0    -1  
$EndComp
Connection ~ 8100 5450
Wire Wire Line
	8100 5450 8100 5550
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR010
U 1 1 5CA7B4F3
P 8400 5750
F 0 "#PWR010" H 8400 5500 50  0001 C CNN
F 1 "GND" H 8405 5577 50  0000 C CNN
F 2 "" H 8400 5750 50  0001 C CNN
F 3 "" H 8400 5750 50  0001 C CNN
	1    8400 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 5450 8400 5450
$Comp
L m5stackoutput_v2-rescue:R-Device R11
U 1 1 5CA81640
P 8400 4450
F 0 "R11" H 8470 4496 50  0000 L CNN
F 1 "100k" H 8470 4405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8330 4450 50  0001 C CNN
F 3 "~" H 8400 4450 50  0001 C CNN
	1    8400 4450
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR09
U 1 1 5CA81647
P 8400 4600
F 0 "#PWR09" H 8400 4350 50  0001 C CNN
F 1 "GND" H 8405 4427 50  0000 C CNN
F 2 "" H 8400 4600 50  0001 C CNN
F 3 "" H 8400 4600 50  0001 C CNN
	1    8400 4600
	1    0    0    -1  
$EndComp
Connection ~ 8100 4300
Wire Wire Line
	8100 4300 8100 4400
$Comp
L m5stackoutput_v2-rescue:R-Device R10
U 1 1 5CA8DC42
P 8400 3250
F 0 "R10" H 8470 3296 50  0000 L CNN
F 1 "100k" H 8470 3205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8330 3250 50  0001 C CNN
F 3 "~" H 8400 3250 50  0001 C CNN
	1    8400 3250
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR08
U 1 1 5CA8DC49
P 8400 3400
F 0 "#PWR08" H 8400 3150 50  0001 C CNN
F 1 "GND" H 8405 3227 50  0000 C CNN
F 2 "" H 8400 3400 50  0001 C CNN
F 3 "" H 8400 3400 50  0001 C CNN
	1    8400 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 4300 8400 4300
Connection ~ 8100 3100
Wire Wire Line
	8100 3100 8100 3200
$Comp
L m5stackoutput_v2-rescue:R-Device R9
U 1 1 5CA9A8DD
P 8400 1950
F 0 "R9" H 8470 1996 50  0000 L CNN
F 1 "100k" H 8470 1905 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 8330 1950 50  0001 C CNN
F 3 "~" H 8400 1950 50  0001 C CNN
	1    8400 1950
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR07
U 1 1 5CA9A8E4
P 8400 2100
F 0 "#PWR07" H 8400 1850 50  0001 C CNN
F 1 "GND" H 8405 1927 50  0000 C CNN
F 2 "" H 8400 2100 50  0001 C CNN
F 3 "" H 8400 2100 50  0001 C CNN
	1    8400 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 3100 8400 3100
Connection ~ 8100 1800
Wire Wire Line
	8100 1800 8100 1900
$Comp
L m5stackoutput_v2-rescue:R-Device R13
U 1 1 5CAA7B1C
P 10300 1950
F 0 "R13" H 10370 1996 50  0000 L CNN
F 1 "100k" H 10370 1905 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10230 1950 50  0001 C CNN
F 3 "~" H 10300 1950 50  0001 C CNN
	1    10300 1950
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR011
U 1 1 5CAA7B23
P 10300 2100
F 0 "#PWR011" H 10300 1850 50  0001 C CNN
F 1 "GND" H 10305 1927 50  0000 C CNN
F 2 "" H 10300 2100 50  0001 C CNN
F 3 "" H 10300 2100 50  0001 C CNN
	1    10300 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 1800 8400 1800
Connection ~ 10000 1800
Wire Wire Line
	10000 1800 10000 1900
$Comp
L m5stackoutput_v2-rescue:R-Device R14
U 1 1 5CAB5189
P 10300 3250
F 0 "R14" H 10370 3296 50  0000 L CNN
F 1 "100k" H 10370 3205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10230 3250 50  0001 C CNN
F 3 "~" H 10300 3250 50  0001 C CNN
	1    10300 3250
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR012
U 1 1 5CAB5190
P 10300 3400
F 0 "#PWR012" H 10300 3150 50  0001 C CNN
F 1 "GND" H 10305 3227 50  0000 C CNN
F 2 "" H 10300 3400 50  0001 C CNN
F 3 "" H 10300 3400 50  0001 C CNN
	1    10300 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 1800 10300 1800
Connection ~ 10000 3100
Wire Wire Line
	10000 3100 10000 3200
$Comp
L m5stackoutput_v2-rescue:R-Device R15
U 1 1 5CAC9B35
P 10300 4450
F 0 "R15" H 10370 4496 50  0000 L CNN
F 1 "100k" H 10370 4405 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10230 4450 50  0001 C CNN
F 3 "~" H 10300 4450 50  0001 C CNN
	1    10300 4450
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR013
U 1 1 5CAC9B3C
P 10300 4600
F 0 "#PWR013" H 10300 4350 50  0001 C CNN
F 1 "GND" H 10305 4427 50  0000 C CNN
F 2 "" H 10300 4600 50  0001 C CNN
F 3 "" H 10300 4600 50  0001 C CNN
	1    10300 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 3100 10300 3100
Connection ~ 10000 4300
Wire Wire Line
	10000 4300 10000 4400
$Comp
L m5stackoutput_v2-rescue:R-Device R16
U 1 1 5CAD7844
P 10300 5600
F 0 "R16" H 10370 5646 50  0000 L CNN
F 1 "100k" H 10370 5555 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10230 5600 50  0001 C CNN
F 3 "~" H 10300 5600 50  0001 C CNN
	1    10300 5600
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR014
U 1 1 5CAD784B
P 10300 5750
F 0 "#PWR014" H 10300 5500 50  0001 C CNN
F 1 "GND" H 10305 5577 50  0000 C CNN
F 2 "" H 10300 5750 50  0001 C CNN
F 3 "" H 10300 5750 50  0001 C CNN
	1    10300 5750
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 4300 10300 4300
Connection ~ 10000 5450
Wire Wire Line
	10000 5450 10000 5550
Wire Wire Line
	10000 5450 10300 5450
Wire Notes Line
	9100 1000 9100 6000
Wire Notes Line
	7200 1000 7200 6000
$Comp
L m5stackoutput_v2-rescue:BSS84AKS-m5stackoutput Q1
U 1 1 5CA40183
P 8000 1550
F 0 "Q1" H 8206 1596 50  0000 L CNN
F 1 "BSS84AKS" H 8206 1505 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 8050 1550 50  0001 C CNN
F 3 "~" H 8050 1550 50  0001 C CNN
	1    8000 1550
	1    0    0    1   
$EndComp
Connection ~ 7800 1550
$Comp
L m5stackoutput_v2-rescue:BSS84AKS-m5stackoutput Q1
U 2 1 5CA40276
P 8000 2850
F 0 "Q1" H 8206 2896 50  0000 L CNN
F 1 "BSS84AKS" H 8206 2805 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 8050 2850 50  0001 C CNN
F 3 "~" H 8050 2850 50  0001 C CNN
	2    8000 2850
	1    0    0    1   
$EndComp
Connection ~ 7800 2850
$Comp
L m5stackoutput_v2-rescue:BSS84AKS-m5stackoutput Q2
U 2 1 5CA4042A
P 8000 5200
F 0 "Q2" H 8206 5246 50  0000 L CNN
F 1 "BSS84AKS" H 8206 5155 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 8050 5200 50  0001 C CNN
F 3 "~" H 8050 5200 50  0001 C CNN
	2    8000 5200
	1    0    0    1   
$EndComp
Connection ~ 7800 5200
$Comp
L m5stackoutput_v2-rescue:BSS84AKS-m5stackoutput Q2
U 1 1 5CA40494
P 8000 4050
F 0 "Q2" H 8206 4096 50  0000 L CNN
F 1 "BSS84AKS" H 8206 4005 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 8050 4050 50  0001 C CNN
F 3 "~" H 8050 4050 50  0001 C CNN
	1    8000 4050
	1    0    0    1   
$EndComp
Connection ~ 7800 4050
$Comp
L m5stackoutput_v2-rescue:BSS84AKS-m5stackoutput Q3
U 1 1 5CA4053A
P 9900 1550
F 0 "Q3" H 10106 1596 50  0000 L CNN
F 1 "BSS84AKS" H 10106 1505 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 9950 1550 50  0001 C CNN
F 3 "~" H 9950 1550 50  0001 C CNN
	1    9900 1550
	1    0    0    1   
$EndComp
$Comp
L m5stackoutput_v2-rescue:BSS84AKS-m5stackoutput Q4
U 1 1 5CA405F0
P 9900 4050
F 0 "Q4" H 10106 4096 50  0000 L CNN
F 1 "BSS84AKS" H 10106 4005 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 9950 4050 50  0001 C CNN
F 3 "~" H 9950 4050 50  0001 C CNN
	1    9900 4050
	1    0    0    1   
$EndComp
Connection ~ 9700 4050
$Comp
L m5stackoutput_v2-rescue:BSS84AKS-m5stackoutput Q4
U 2 1 5CA406BC
P 9900 5200
F 0 "Q4" H 10106 5246 50  0000 L CNN
F 1 "BSS84AKS" H 10106 5155 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 9950 5200 50  0001 C CNN
F 3 "~" H 9950 5200 50  0001 C CNN
	2    9900 5200
	1    0    0    1   
$EndComp
Connection ~ 9700 5200
$Comp
L m5stackoutput_v2-rescue:BSS84AKS-m5stackoutput Q3
U 2 1 5CA4074A
P 9900 2850
F 0 "Q3" H 10106 2896 50  0000 L CNN
F 1 "BSS84AKS" H 10106 2805 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-363_SC-70-6" H 9950 2850 50  0001 C CNN
F 3 "~" H 9950 2850 50  0001 C CNN
	2    9900 2850
	1    0    0    1   
$EndComp
Connection ~ 9700 2850
Wire Notes Line
	9100 1000 10700 1000
Wire Notes Line
	10700 1000 10700 6000
Wire Notes Line
	10700 6000 9100 6000
Wire Notes Line
	7200 6000 8800 6000
Wire Notes Line
	8800 6000 8800 1000
Wire Notes Line
	8800 1000 7200 1000
Wire Notes Line
	3250 6350 6550 6350
Wire Wire Line
	4600 5600 4800 5600
Wire Wire Line
	4750 3100 5200 3100
$Comp
L m5stackoutput_v2-rescue:C-Device C1
U 1 1 5CAAC066
P 5700 3100
F 0 "C1" V 5448 3100 50  0000 C CNN
F 1 "C" V 5539 3100 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5738 2950 50  0001 C CNN
F 3 "~" H 5700 3100 50  0001 C CNN
	1    5700 3100
	0    1    1    0   
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR0101
U 1 1 5CAAC163
P 5950 3100
F 0 "#PWR0101" H 5950 2850 50  0001 C CNN
F 1 "GND" H 5955 2927 50  0000 C CNN
F 2 "" H 5950 3100 50  0001 C CNN
F 3 "" H 5950 3100 50  0001 C CNN
	1    5950 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5950 3100 5850 3100
Text GLabel 4750 4750 0    50   Input ~ 0
5V
Wire Wire Line
	5200 4750 5200 4900
Wire Wire Line
	4750 4750 5200 4750
$Comp
L m5stackoutput_v2-rescue:C-Device C2
U 1 1 5CAB1F12
P 5800 4750
F 0 "C2" V 5548 4750 50  0000 C CNN
F 1 "C" V 5639 4750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5838 4600 50  0001 C CNN
F 3 "~" H 5800 4750 50  0001 C CNN
	1    5800 4750
	0    1    1    0   
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR0102
U 1 1 5CAB1F1A
P 6050 4750
F 0 "#PWR0102" H 6050 4500 50  0001 C CNN
F 1 "GND" H 6055 4577 50  0000 C CNN
F 2 "" H 6050 4750 50  0001 C CNN
F 3 "" H 6050 4750 50  0001 C CNN
	1    6050 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 4750 5950 4750
Wire Wire Line
	5200 4750 5650 4750
Connection ~ 5200 4750
Wire Wire Line
	5550 3100 5200 3100
Connection ~ 5200 3100
$Comp
L m5stackoutput_v2-rescue:SolderJumper_3_Open-Jumper JP3
U 1 1 5CB1DB6E
P 3800 4150
F 0 "JP3" V 3650 4150 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 3755 4218 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 3800 4150 50  0001 C CNN
F 3 "~" H 3800 4150 50  0001 C CNN
	1    3800 4150
	0    -1   -1   0   
$EndComp
$Comp
L m5stackoutput_v2-rescue:SolderJumper_3_Open-Jumper JP2
U 1 1 5CC1E330
P 3800 3750
F 0 "JP2" V 3650 3750 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 3755 3818 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 3800 3750 50  0001 C CNN
F 3 "~" H 3800 3750 50  0001 C CNN
	1    3800 3750
	0    -1   -1   0   
$EndComp
$Comp
L m5stackoutput_v2-rescue:SolderJumper_3_Open-Jumper JP1
U 1 1 5CC6B89B
P 3800 3350
F 0 "JP1" V 3650 3350 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 3755 3418 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 3800 3350 50  0001 C CNN
F 3 "~" H 3800 3350 50  0001 C CNN
	1    3800 3350
	0    -1   -1   0   
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR0104
U 1 1 5CCC7ADE
P 3800 4350
F 0 "#PWR0104" H 3800 4100 50  0001 C CNN
F 1 "GND" H 3805 4177 50  0000 C CNN
F 2 "" H 3800 4350 50  0001 C CNN
F 3 "" H 3800 4350 50  0001 C CNN
	1    3800 4350
	1    0    0    -1  
$EndComp
Text GLabel 3500 3100 0    50   Input ~ 0
5V
Wire Wire Line
	3500 3100 3600 3100
Wire Wire Line
	3800 3100 3800 3150
Wire Wire Line
	3950 3350 4250 3350
Wire Wire Line
	4250 3350 4250 3650
Wire Wire Line
	4250 3650 4800 3650
Wire Wire Line
	4800 3750 3950 3750
Wire Wire Line
	3950 4150 4250 4150
Wire Wire Line
	4250 4150 4250 3850
Wire Wire Line
	4250 3850 4800 3850
Wire Wire Line
	3600 3100 3600 3950
Wire Wire Line
	3600 3950 3800 3950
Connection ~ 3600 3100
Wire Wire Line
	3600 3100 3800 3100
Connection ~ 3800 3950
Wire Wire Line
	3500 3550 3800 3550
Connection ~ 3800 3550
Wire Wire Line
	3800 4350 3500 4350
Wire Wire Line
	3500 3550 3500 4350
Connection ~ 3800 4350
Text Notes 4000 4650 0    50   ~ 0
I2C Port Expander 8 Bit each
$Comp
L m5stackoutput_v2-rescue:SolderJumper_3_Open-Jumper JP6
U 1 1 5CABA5BA
P 3800 5800
F 0 "JP6" V 3650 5800 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 3755 5868 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 3800 5800 50  0001 C CNN
F 3 "~" H 3800 5800 50  0001 C CNN
	1    3800 5800
	0    -1   -1   0   
$EndComp
$Comp
L m5stackoutput_v2-rescue:SolderJumper_3_Open-Jumper JP5
U 1 1 5CABA5C1
P 3800 5400
F 0 "JP5" V 3650 5400 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 3755 5468 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 3800 5400 50  0001 C CNN
F 3 "~" H 3800 5400 50  0001 C CNN
	1    3800 5400
	0    -1   -1   0   
$EndComp
$Comp
L m5stackoutput_v2-rescue:SolderJumper_3_Open-Jumper JP4
U 1 1 5CABA5C8
P 3800 5000
F 0 "JP4" V 3650 5000 50  0000 L CNN
F 1 "SolderJumper_3_Open" V 3755 5068 50  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_Pad1.0x1.5mm_NumberLabels" H 3800 5000 50  0001 C CNN
F 3 "~" H 3800 5000 50  0001 C CNN
	1    3800 5000
	0    -1   -1   0   
$EndComp
$Comp
L m5stackoutput_v2-rescue:GND-power #PWR02
U 1 1 5CABA5CF
P 3800 6000
F 0 "#PWR02" H 3800 5750 50  0001 C CNN
F 1 "GND" H 3805 5827 50  0000 C CNN
F 2 "" H 3800 6000 50  0001 C CNN
F 3 "" H 3800 6000 50  0001 C CNN
	1    3800 6000
	1    0    0    -1  
$EndComp
Text GLabel 3500 4750 0    50   Input ~ 0
5V
Wire Wire Line
	3500 4750 3600 4750
Wire Wire Line
	3800 4750 3800 4800
Wire Wire Line
	3950 5000 4250 5000
Wire Wire Line
	4800 5400 3950 5400
Wire Wire Line
	3950 5800 4250 5800
Wire Wire Line
	3600 4750 3600 5600
Wire Wire Line
	3600 5600 3800 5600
Connection ~ 3600 4750
Wire Wire Line
	3600 4750 3800 4750
Connection ~ 3800 5600
Wire Wire Line
	3500 5200 3800 5200
Connection ~ 3800 5200
Wire Wire Line
	3800 6000 3500 6000
Wire Wire Line
	3500 5200 3500 6000
Connection ~ 3800 6000
Wire Wire Line
	4250 5000 4250 5300
Wire Wire Line
	4250 5300 4800 5300
Wire Wire Line
	4250 5800 4250 5500
Wire Wire Line
	4250 5500 4800 5500
Wire Wire Line
	1400 7100 1400 7200
Wire Wire Line
	1400 7200 1500 7200
Connection ~ 1400 7100
Wire Wire Line
	1400 7100 1500 7100
Wire Wire Line
	1400 7200 1400 7300
Wire Wire Line
	1400 7300 1500 7300
Connection ~ 1400 7200
Wire Notes Line
	6550 2800 6550 6350
Wire Notes Line
	3250 2800 3250 6350
$Comp
L m5stackoutput_v2-rescue:Conn_01x08-Connector_Generic J6
U 1 1 5D47609D
P 3850 7450
F 0 "J6" V 3722 7830 50  0000 L CNN
F 1 "Conn_01x08" V 3813 7830 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x04_P2.54mm_Vertical" H 3850 7450 50  0001 C CNN
F 3 "~" H 3850 7450 50  0001 C CNN
	1    3850 7450
	0    1    1    0   
$EndComp
Text GLabel 4150 6750 1    50   Input ~ 0
GND
Text GLabel 3450 6750 1    50   Input ~ 0
3,3V
Text GLabel 3650 6750 1    50   Input ~ 0
GPIO1
Text GLabel 3850 6750 1    50   Input ~ 0
GPIO2
Text GLabel 3550 6750 1    50   Input ~ 0
MOSI
Text GLabel 3750 6750 1    50   Input ~ 0
MISO
Text GLabel 3950 6750 1    50   Input ~ 0
SCLK
Text GLabel 4050 6750 1    50   Input ~ 0
GPIO3
Wire Wire Line
	3550 6950 3550 6750
Wire Wire Line
	3750 6950 3750 6750
Wire Wire Line
	1200 1150 1200 1050
Wire Wire Line
	1450 1150 1200 1150
Wire Wire Line
	1450 1050 1200 1050
Connection ~ 1200 1050
Wire Wire Line
	1200 1050 1200 950 
Wire Wire Line
	1450 950  1200 950 
Text GLabel 1100 1450 0    50   Input ~ 0
SCLK
Text GLabel 1100 1350 0    50   Input ~ 0
MISO
Text GLabel 1100 1250 0    50   Input ~ 0
MOSI
Wire Wire Line
	1100 1250 1450 1250
Wire Wire Line
	1100 1350 1450 1350
Wire Wire Line
	1100 1450 1450 1450
Text GLabel 1100 1850 0    50   Input ~ 0
GPIO1
Text GLabel 2250 1850 2    50   Input ~ 0
GPIO2
Wire Wire Line
	1100 1850 1450 1850
Wire Wire Line
	2250 1850 1950 1850
$Comp
L m5stackoutput_v2-rescue:R-Device R17
U 1 1 5D2662CB
P 3550 7100
F 0 "R17" H 3620 7146 50  0000 L CNN
F 1 "330" H 3620 7055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3480 7100 50  0001 C CNN
F 3 "~" H 3550 7100 50  0001 C CNN
	1    3550 7100
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:R-Device R18
U 1 1 5D2668C5
P 3750 7100
F 0 "R18" H 3820 7146 50  0000 L CNN
F 1 "330" H 3820 7055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3680 7100 50  0001 C CNN
F 3 "~" H 3750 7100 50  0001 C CNN
	1    3750 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 6750 3650 7250
Wire Wire Line
	3450 6750 3450 7250
Wire Wire Line
	3850 6750 3850 7250
Wire Wire Line
	4050 6750 4050 7250
Wire Wire Line
	3950 6750 3950 7250
Wire Wire Line
	4150 6750 4150 7250
$Comp
L m5stackoutput_v2-rescue:MountingHole_Pad-Mechanical H1
U 1 1 5D2B624C
P 6100 7150
F 0 "H1" H 6200 7199 50  0000 L CNN
F 1 "MountingHole_Pad" H 6200 7108 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.85mm" H 6100 7150 50  0001 C CNN
F 3 "~" H 6100 7150 50  0001 C CNN
	1    6100 7150
	1    0    0    -1  
$EndComp
Text GLabel 6100 7250 3    50   Input ~ 0
5V
$Comp
L m5stackoutput_v2-rescue:MountingHole_Pad-Mechanical H2
U 1 1 5D2B846B
P 5900 7150
F 0 "H2" H 6000 7199 50  0000 L CNN
F 1 "MountingHole_Pad" H 6000 7108 50  0000 L CNN
F 2 "TestPoint:TestPoint_Loop_D2.50mm_Drill1.85mm" H 5900 7150 50  0001 C CNN
F 3 "~" H 5900 7150 50  0001 C CNN
	1    5900 7150
	1    0    0    -1  
$EndComp
Text GLabel 5900 7250 3    50   Input ~ 0
GND
Wire Wire Line
	1000 1000 1000 950 
Wire Wire Line
	1000 950  1200 950 
Connection ~ 1200 950 
Text GLabel 4100 1200 3    50   Input ~ 0
GND
Text GLabel 4400 1200 3    50   Input ~ 0
RX
Text GLabel 4500 1200 3    50   Input ~ 0
TX
Text GLabel 1450 1550 0    50   Input ~ 0
RX
Text GLabel 1950 1550 2    50   Input ~ 0
TX
$Comp
L m5stackoutput_v2-rescue:LED-Device D1
U 1 1 5D3427DF
P 4500 2500
F 0 "D1" H 4493 2245 50  0000 C CNN
F 1 "LED" H 4493 2336 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4500 2500 50  0001 C CNN
F 3 "~" H 4500 2500 50  0001 C CNN
	1    4500 2500
	-1   0    0    1   
$EndComp
$Comp
L m5stackoutput_v2-rescue:R-Device R19
U 1 1 5D342F36
P 4200 2500
F 0 "R19" V 3993 2500 50  0000 C CNN
F 1 "10k" V 4084 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4130 2500 50  0001 C CNN
F 3 "~" H 4200 2500 50  0001 C CNN
	1    4200 2500
	0    1    1    0   
$EndComp
Text GLabel 4950 2500 2    50   Input ~ 0
GND
Wire Wire Line
	4650 2500 4950 2500
Text GLabel 2450 2250 2    50   Input ~ 0
5V
Text GLabel 3750 2500 0    50   Input ~ 0
5V
Wire Wire Line
	3750 2500 4050 2500
Connection ~ 9700 1550
Text GLabel 7650 3600 0    50   Input ~ 0
5V
Text GLabel 7650 4750 0    50   Input ~ 0
5V
Text GLabel 9550 4750 0    50   Input ~ 0
5V
Text GLabel 9550 3600 0    50   Input ~ 0
5V
Text GLabel 9550 2400 0    50   Input ~ 0
5V
Text GLabel 9500 1100 0    50   Input ~ 0
5V
Text GLabel 7650 2400 0    50   Input ~ 0
5V
Text GLabel 7600 1100 0    50   Input ~ 0
5V
Wire Notes Line
	7100 600  7100 2050
Wire Notes Line
	3500 2050 7100 2050
Wire Notes Line
	3500 2050 3500 600 
Wire Notes Line
	7100 600  3500 600 
Text Notes 5000 950  0    50   ~ 0
Serial
Text Notes 4200 2200 0    50   ~ 0
Power LED
Text Notes 3600 7600 0    50   ~ 0
Card Reader
Text Notes 5950 6900 0    50   ~ 0
PWR
Text GLabel 1950 1450 2    50   Input ~ 0
3,3V
Wire Wire Line
	1050 5400 1500 5400
Text GLabel 1050 5500 0    50   Input ~ 0
5V
Wire Wire Line
	1050 5500 1250 5500
Wire Wire Line
	1250 5500 1250 5600
Wire Wire Line
	1250 5600 1500 5600
Connection ~ 1250 5500
Wire Wire Line
	1250 5500 1500 5500
Wire Wire Line
	1050 3750 1500 3750
Text GLabel 1050 3850 0    50   Input ~ 0
3,3V
Wire Wire Line
	1050 3850 1400 3850
Wire Wire Line
	1400 3850 1400 3950
Wire Wire Line
	1400 3950 1500 3950
Connection ~ 1400 3850
Wire Wire Line
	1400 3850 1500 3850
Text Notes 4800 750  0    50   ~ 0
RX TX DTR CTS für Auto Upload
Text GLabel 2250 1350 2    50   Input ~ 0
GPIO3
Wire Wire Line
	2250 1350 1950 1350
Text GLabel 2250 1150 2    50   Input ~ 0
Reset
Wire Wire Line
	1950 1150 2250 1150
Text GLabel 2250 2050 2    50   Input ~ 0
G0
Wire Wire Line
	1950 2050 2250 2050
$Comp
L m5stackoutput_v2-rescue:Conn_01x06-Connector_Generic J4
U 1 1 5D52B182
P 4300 1000
F 0 "J4" V 4264 612 50  0000 R CNN
F 1 "Conn_01x06" V 4173 612 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Vertical" H 4300 1000 50  0001 C CNN
F 3 "~" H 4300 1000 50  0001 C CNN
	1    4300 1000
	0    -1   -1   0   
$EndComp
Text GLabel 4300 1200 3    50   Input ~ 0
3,3V
Text GLabel 4200 1200 3    50   Input ~ 0
CTS
Text GLabel 4600 1200 3    50   Input ~ 0
DTR
$Comp
L m5stackoutput_v2-rescue:Q_NPN_BEC-Device Q5
U 1 1 5D551484
P 6450 950
F 0 "Q5" H 6641 996 50  0000 L CNN
F 1 "BCX19" H 6641 905 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6650 1050 50  0001 C CNN
F 3 "~" H 6450 950 50  0001 C CNN
	1    6450 950 
	1    0    0    -1  
$EndComp
$Comp
L m5stackoutput_v2-rescue:Q_NPN_BEC-Device Q6
U 1 1 5D553E96
P 6450 1650
F 0 "Q6" H 6641 1604 50  0000 L CNN
F 1 "BCX19" H 6641 1695 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6650 1750 50  0001 C CNN
F 3 "~" H 6450 1650 50  0001 C CNN
	1    6450 1650
	1    0    0    1   
$EndComp
Text GLabel 5500 950  0    50   Input ~ 0
DTR
Text GLabel 5550 1650 0    50   Input ~ 0
CTS
Wire Wire Line
	5550 1650 5700 1650
Wire Wire Line
	6100 1650 6250 1650
Wire Wire Line
	5500 950  5650 950 
Wire Wire Line
	6050 950  6250 950 
Wire Wire Line
	6550 1450 6550 1350
Connection ~ 5650 950 
Wire Wire Line
	5650 950  5750 950 
Wire Wire Line
	5650 950  5650 1350
Wire Wire Line
	5650 1350 6550 1350
Wire Wire Line
	6550 1150 6550 1250
Wire Wire Line
	6550 1250 5700 1250
Wire Wire Line
	5700 1250 5700 1650
Connection ~ 5700 1650
Wire Wire Line
	5700 1650 5800 1650
Wire Wire Line
	6700 700  6550 700 
Wire Wire Line
	6550 700  6550 750 
Text GLabel 6700 700  2    50   Input ~ 0
Reset
Text GLabel 6700 1950 2    50   Input ~ 0
G0
Wire Wire Line
	6700 1950 6550 1950
Wire Wire Line
	6550 1950 6550 1850
$Comp
L m5stackoutput_v2-rescue:R-Device R20
U 1 1 5D6AA998
P 5900 950
F 0 "R20" H 5970 996 50  0000 L CNN
F 1 "10k" H 5970 905 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5830 950 50  0001 C CNN
F 3 "~" H 5900 950 50  0001 C CNN
	1    5900 950 
	0    -1   -1   0   
$EndComp
$Comp
L m5stackoutput_v2-rescue:R-Device R21
U 1 1 5D6AAEF8
P 5950 1650
F 0 "R21" H 6020 1696 50  0000 L CNN
F 1 "10k" H 6020 1605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5880 1650 50  0001 C CNN
F 3 "~" H 5950 1650 50  0001 C CNN
	1    5950 1650
	0    -1   -1   0   
$EndComp
Wire Notes Line
	3200 7600 4750 7600
Wire Notes Line
	4750 7600 4750 6450
Wire Notes Line
	3200 6450 4750 6450
Wire Notes Line
	3200 7600 3200 6450
Wire Notes Line
	5250 2700 3550 2700
Wire Notes Line
	5250 2100 3550 2100
Wire Notes Line
	3550 2100 3550 2700
Wire Notes Line
	5250 2100 5250 2700
$EndSCHEMATC
